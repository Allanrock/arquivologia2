<?php
include 'db.php';
?>
<!DOCTYPE html>
<html lang = "en">
    <head>
        <meta charset = "utf-8">
        <link href = "bootstrap/css/bootstrap.min.css" rel = "stylesheet">
        <script src = "bootstrap/js/bootstrap.min.js"></script>
    </head>

    <body>
        <?php include 'menu.php' ?>
        <br/><br/><br/>
        <div class="container">  
            <div class="row">  
                <div class="col-md-4 col-md-offset-4">  
                    <div class="login-panel panel panel-success">  
                        <div class="panel-heading">  
                            <h3 class="panel-title" >Escolha uma caixa</h3>  
                        </div>  
                        <div class="panel-body">  
                            <form role="form" method="get" action="mostra_documentos.php">  
                                <fieldset>  
                                    <div class="form-group">
                                        <label for="caixa">Caixa</label>
                                        <select class="form-control" name="caixa" id="caixa">
                                            <?php
                                            $busca = "select * from caixa";
                                            foreach ($PDO->query($busca) as $doc) {
                                                ?>
                                            <option class="form-control" id="idcaixa" value="<?php echo $doc["idcaixa"] ?>" name="idcaixa" id="idcaixa"><?php echo $doc["idcaixa"] ?></option>
                                            <?php } ?>
                                        </select>
                                        <br/>
                                        <input class="btn btn-lg btn-success btn-block" type="submit" value="Escolher" name="login" > 
                                    </div>
                                </fieldset>  
                            </form>  
                        </div>  
                    </div>  
                </div>  
            </div>  
        </div>

    </body>
</html>