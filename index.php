<?php
if (!empty($_POST)) {
    require 'db.php';
    $nome = $_POST['usuario'];
    $senha = $_POST['senha'];
    $email = $_POST['email'];

    $sql = "insert into usuarios (usuario,senha,email) values (?,?,?)";
    $PDO->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $stm = $PDO->prepare($sql);
    if ($stm->execute(array($nome, $senha, $email))) {
        ?> <script>alert("Usuário cadastrado com sucesso")</script><?php
        header("Location: login.php");
    } else {
        ?> <script>alert("Impossível cadastrar")</script>
        <?php
        header("Location: index.php");
    }
}
    ?>
<!doctype html>
<html>  
    <head lang="en">  
        <meta charset="UTF-8">  
        <link type="text/css" rel="stylesheet" href="bootstrap\css\bootstrap.css">  
        <title>Registration</title>  
    </head>  
    <style>  
        .login-panel {  
            margin-top: 150px;  

        </style>  
        <body>  

            <div class="container"><!-- container class is used to centered  the body of the browser with some decent width-->  
                <div class="row"><!-- row class is used for grid system in Bootstrap-->  
                    <div class="col-md-4 col-md-offset-4"><!--col-md-4 is used to create the no of colums in the grid also use for medimum and large devices-->  
                        <div class="login-panel panel panel-success">  
                            <div class="panel-heading">  
                                <h3 class="panel-title">Cadastro</h3>  
                            </div>  
                            <div class="panel-body">  
                                <form role="form" method="post" action="">  
                                    <fieldset>  
                                        <div class="form-group">  
                                            <input class="form-control" placeholder="Usuario" name="usuario" type="text" autofocus>  
                                        </div>  

                                        <div class="form-group">  
                                            <input class="form-control" placeholder="E-mail" name="email" type="email" autofocus>  
                                        </div>  
                                        <div class="form-group">  
                                            <input class="form-control" placeholder="Senha" name="senha" type="password" value="">  
                                        </div>  


                                        <input class="btn btn-lg btn-success btn-block" type="submit" value="Cadastrar" name="Cadastrar" >  

                                    </fieldset>  
                                </form>  
                                <center><b>Ja é Cadastrado ?</b> <br></b><a href="login.php">Faça ja o login</a></center><!--for centered text-->  
                            </div>  
                        </div>  
                    </div>  
                </div>  
            </div>  

        </body>  

    </html>  