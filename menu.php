
<!DOCTYPE html>
<html lang = "en">
    <head>
        <meta charset = "utf-8">
        <link href = "bootstrap/css/bootstrap.min.css" rel = "stylesheet">
        <script src = "bootstrap/js/bootstrap.min.js"></script>
        <style>
            #menu ul {
                padding:0px;
                margin:0px;
                background-color:#EDEDED;
                list-style:none;
            }

            #menu ul li { display: inline; margin-left: 50px;}

            #menu ul li a {
                padding: 2px 10px;
                display: inline-block;

                /* visual do link */
                background-color:#EDEDED;
                color: #333;
                text-decoration: none;
                border-bottom:3px solid #EDEDED;
            }

            #menu ul li a:hover {
                background-color:#D6D6D6;
                color: #6D6D6D;
                border-bottom:3px solid #EA0000;
            }
        </style>
    </head>

    <body>

        <nav id="menu">
            <ul>
                <li><a href="insere_caixa.php">Cadastrar Caixa</a></li>
                <li><a href="insere_documento.php">Cadastrar Documento</a></li>
                <li><a href="cadastra_usuario.php">Cadastrar Usuário</a></li>
                <li><a href="escolha.php">Mostrar Documentos</a></li>                
                <li id="logt"><a href="logout.php">Logout</a></li>
            </ul>
        </nav>
