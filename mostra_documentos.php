<!DOCTYPE html>
<html lang = "en">
    <head>
        <meta charset = "utf-8">
        <link href = "bootstrap/css/bootstrap.min.css" rel = "stylesheet">
        <script src = "bootstrap/js/bootstrap.min.js"></script>
    </head>

    <body>
        <?php include 'menu.php' ?>
        <div class="container">

            <div class="row">
                <div class="row">
                    <h3>Documentos</h3>
                    <img src="http://chart.apis.google.com/chart?cht=qr&chs=250x250&chl=http://arquivologia.azurewebsites.net/mostra_documentos.php?caixa=<?php echo $_GET["caixa"]?>&login=Escolher" alt="QRCODE CAIXA<?php echo $_GET["caixa"]?>">
                </div>

                <div class="row">
                    <form method="POST" action="">
                        <?php
                        include 'db.php';
                        $id = $_GET["caixa"];
                        $busca = "select * from documento where caixa_idcaixa = $id";
                        foreach ($PDO->query($busca) as $doc) {
                            ?>
                            <div class="form-group">
                                <input type="hidden" name="iddocumento" value="<?php echo $doc['iddocumento'] ?>"
                                       <label>Número da caixa</label>
                                <input type="text" name="caixa_id" value="<?php echo $doc['caixa_idcaixa']; ?>"/>
                            </div>
                            <div class="form-group".
                                 <label>Número do Documento</label>
                                <input type="text" name="num_doc" value="<?php echo $doc['num_doc_proc']; ?>"/>
                            </div>
                            <div class="form-group">
                                <label>Nome do Curso</label>
                                <input type="text" name="nome_curso" value="<?php echo $doc['nome_curso']; ?>"/>
                            </div>
                            <div class="form-group">
                                <label>Título do Documento</label>
                                <input type="text" name="titulo_doc" value="<?php echo $doc['titulo_doc']; ?>"/>
                            </div>
                            <div class="form-group">
                                <label>Autor do Documento</label>
                                <input type="text" name="autor_doc" value="<?php echo $doc['autor_doc']; ?>"/>
                            </div>
                            <div class="form-group">
                                <label>Procedência</label>
                                <input type="text" name="procedencia" value="<?php echo $doc['procedencia']; ?>"/>
                            </div>
                            <div class="form-group">
                                <label>Destinatário</label>
                                <input type="text" name="destinatario" value="<?php echo $doc['destinatario']; ?>"/>
                            </div>
                            <div class="form-group">
                                <label>Setor da Instituição</label>
                                <input type="text" name="setor_inst" value="<?php echo $doc['setor_instituicao']; ?>"/>
                            </div>
                            <div class="form-group">
                                <label>Órgão Executor</label>
                                <input type="text" name="org_executor" value="<?php echo $doc['org_executor']; ?>"/>
                            </div>
                            <div class="form-group">
                                <label>Tipo documental</label>
                                <input type="text" name="tipo_documento" value="<?php echo $doc['tipo_documental']; ?>"/>
                            </div>
                            <div class="form-group">
                                <label>Nível de Acesso</label>
                                <input type="text" name="nivel_acesso" value="<?php echo $doc['nivel_acesso']; ?>"/>
                            </div>
                            <div class="form-group">
                                <label>Data Cronológica</label>
                                <input type="date" name="dt_crono" value="<?php echo $doc['dt_cronologica']; ?>"/>
                            </div>
                            <div class="form-group">
                                <label>Data Tópica</label>
                                <input type="date" name="dt_tipica" value="<?php echo $doc['dt_topica']; ?>"/>
                            </div>
                            <div class="form-group">
                                <label>Classe Documental</label>
                                <input type="text" name="classe_doc" value="<?php echo $doc['classe_documental']; ?>"/>
                            </div>
                            <div class="form-actions">
                                <button type="submit" class="btn btn-success" value="editar" name="editar">Editar</button>
                                <button type="submit" class="btn btn-success" value="excluir" name="excluir">Excluir</button>
                                <a class="btn btn-default" href="escolha.php">Voltar</a>
                            </div>
                            <hr/>
                        <?php } ?>
                    </form>
                </div>

            </div>
        </div>
    </body>
</html>

<?php
if (isset($_POST['editar'])) {
    $id_documento = $_POST['iddocumento'];
    $caixa_id = $_POST['caixa_id'];
    $num_doc = $_POST['num_doc'];
    $nome_curso = $_POST['nome_curso'];
    $titulo_doc = $_POST['titulo_doc'];
    $autor_doc = $_POST['autor_doc'];
    $procedencia = $_POST['procedencia'];
    $destinatario = $_POST['destinatario'];
    $setor_inst = $_POST['setor_inst'];
    $org_executor = $_POST['org_executor'];
    $tipo_documento = $_POST['tipo_documento'];
    $nivel_acesso = $_POST['nivel_acesso'];
    $dt_crono = $_POST['dt_crono'];
    $dt_tipica = $_POST['dt_tipica'];
    $classe_doc = $_POST['classe_doc'];

    $PDO->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $sqlUpdate = "UPDATE documento SET caixa_idcaixa = :caixa_id, 
            num_doc_proc = :num_doc, 
            nome_curso = :nome_curso,  
            titulo_doc = :titulo_doc,  
            autor_doc = :autor_doc,
            procedencia = :procedencia,
            destinatario = :destinatario,
            setor_instrucao = :setor_inst,
            org_executor = :org_executor,
            tipo_documental = :tipo_documento,
            nivel_acesso = :nivel_acesso,
            dt_cronologica = :dt_crono,
            dt_topica = :dt_tipica,
            classe_documental = :classe_doc
            WHERE iddocumento = :iddocumento";
    $stmt = $PDO->prepare($sqlUpdate);
    $stmt->bindParam(':caixa_id', $_POST['caixa_id'], PDO::PARAM_INT);
    $stmt->bindParam(':num_doc', $_POST['num_doc'], PDO::PARAM_STR);
    $stmt->bindParam(':nome_curso', $_POST['nome_curso'], PDO::PARAM_STR);
// use PARAM_STR although a number  
    $stmt->bindParam(':titulo_doc', $_POST['titulo_doc'], PDO::PARAM_STR);
    $stmt->bindParam(':autor_doc', $_POST['autor_doc'], PDO::PARAM_STR);
    $stmt->bindParam(':autor_doc', $_POST['procedencia'], PDO::PARAM_STR);
    $stmt->bindParam(':destinatario', $_POST['destinatario'], PDO::PARAM_STR);
    $stmt->bindParam(':setor_inst', $_POST['setor_inst'], PDO::PARAM_STR);
    $stmt->bindParam(':org_executor', $_POST['org_executor'], PDO::PARAM_STR);
    $stmt->bindParam(':tipo_documento', $_POST['tipo_documento'], PDO::PARAM_STR);
    $stmt->bindParam(':nivel_acesso', $_POST['nivel_acesso'], PDO::PARAM_STR);
    $stmt->bindParam(':dt_crono', $_POST['dt_crono'], PDO::PARAM_STR);
    $stmt->bindParam(':dt_tipica', $_POST['dt_tipica'], PDO::PARAM_STR);
    $stmt->bindParam(':classe_doc', $_POST['classe_doc'], PDO::PARAM_STR);
    $stmt->bindParam(':iddocumento', $_POST['iddocumento'], PDO::PARAM_INT);
    $stmt->execute();
    $PDO = null;
    header("Location: index.php");
}

if (isset($_POST['excluir'])) {
    $sqlDelete = "DELETE FROM documento WHERE iddocumento = '$_POST[iddocumento]'";
    $stm_del = $PDO->prepare($sqlDelete);
    $stm_del->bindParam(':iddocumento', $_POST['iddocumento'], PDO::PARAM_INT);
    $stm_del->execute();
}
?>