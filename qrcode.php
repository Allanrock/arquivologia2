<?php
include 'db.php';
?>
<!DOCTYPE html>
<html lang = "en">
    <head>
        <meta charset = "utf-8">
        <link href = "bootstrap/css/bootstrap.min.css" rel = "stylesheet">
        <script src = "bootstrap/js/bootstrap.min.js"></script>
    </head>

    <body>
        <?php include 'menu.php' ?>
        <div class="container">

            <div class="row">
                <div class="row">
                    <h3>Gerar Qrcode</h3>
                </div>

                <form method="POST" action="pegar_qrcode.php">
                    <div class="form-group">
                        <label for="caixa">Caixa</label>
                        <select name="caixa" id="caixa">
                            <?php
                            $busca = "select * from caixa";
                            foreach ($PDO->query($busca) as $doc) {
                                ?>
                                <option class="form-control" id="idcaixa" value="<?php echo $doc["idcaixa"] ?>" name="idcaixa"><?php echo $doc["idcaixa"] ?></option>
                            <?php } ?>
                        </select>

                    </div>
                    <div class="form-actions">
                        <button type="submit" class="btn btn-success">Gerar</button>
                        <a class="btn btn-default" href="index.php">Voltar</a>
                    </div>
                </form>
            </div>
        </div>
    </body>
</html>

