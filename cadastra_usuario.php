<?php
if (!empty($_POST)) {
    require 'db.php';
    $nome = $_POST['usuario'];
    $senha = $_POST['senha'];
    $email = $_POST['email'];

    $sql = "insert into usuarios (usuario,senha,email) values (?,?,?)";
    $PDO->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $stm = $PDO->prepare($sql);
    if ($stm->execute(array($nome, $senha, $email))) {
        ?> <script>alert("Usuário cadastrado com sucesso")</script><?php
        header("Location: login.php");
    } else {
        ?> <script>alert("Impossível cadastrar")</script>
        <?php
        header("Location: index.php");
    }
}
?>
<!DOCTYPE html>
<html lang = "en">
    <head>
        <meta charset = "utf-8">
        <link href = "bootstrap/css/bootstrap.min.css" rel = "stylesheet">
        <script src = "bootstrap/js/bootstrap.min.js"></script>
    </head>
    <body>
        <?php include 'menu.php'; ?>
        <div class="container">

            <div class="row">
                <div class="row">
                    <h3>Cadastrar Usuário</h3>
                </div>

                <form method="POST" action="">
                    <div class="form-group <?php echo!empty($fundo_error) ? 'has-error' : ''; ?>">
                        <label for="nome">Nome</label>
                        <input type="text" class="form-control" id="nome" name="nome">
                    </div>
                    <div class="form-group">
                        <label for="senha">Senha</label>
                        <input type="text" name="senha" id="senha" class="form-control"/>
                    </div>
                    <div class="form-group">
                        <label for="email">Email</label>
                        <input type="text" name="email" id="email" class="form-control"/>
                    </div>
                    <div class="form-actions">
                        <button type="submit" class="btn btn-success">Inserir</button>
                        <a class="btn btn-default" href="index.php">Voltar</a>
                    </div>
                </form>
            </div>
        </div>
    </body>
</html>

