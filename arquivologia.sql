-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Tempo de geração: 06/05/2015 às 21:33
-- Versão do servidor: 5.6.21
-- Versão do PHP: 5.6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Banco de dados: `arquivologia`
--

-- --------------------------------------------------------

--
-- Estrutura para tabela `caixa`
--

CREATE TABLE IF NOT EXISTS `caixa` (
`idcaixa` int(11) NOT NULL,
  `fundo` varchar(150) DEFAULT NULL,
  `secao` varchar(150) DEFAULT NULL,
  `sub_secao` varchar(150) DEFAULT NULL,
  `serie` varchar(150) DEFAULT NULL,
  `funcao` varchar(150) DEFAULT NULL,
  `classe` varchar(150) DEFAULT NULL,
  `tipos_documentos` varchar(100) DEFAULT NULL,
  `caixacol` varchar(150) DEFAULT NULL,
  `descritores` varchar(150) DEFAULT NULL,
  `qrcode` varchar(150) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Fazendo dump de dados para tabela `caixa`
--

INSERT INTO `caixa` (`idcaixa`, `fundo`, `secao`, `sub_secao`, `serie`, `funcao`, `classe`, `tipos_documentos`, `caixacol`, `descritores`, `qrcode`) VALUES
(1, '2', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', ''),
(2, '45', 'OI', 'OI', 'OIO', 'IO', 'IOI', 'O', 'IO', 'IOI', '');

-- --------------------------------------------------------

--
-- Estrutura para tabela `documento`
--

CREATE TABLE IF NOT EXISTS `documento` (
`iddocumento` int(11) NOT NULL,
  `caixa_idcaixa` int(11) NOT NULL,
  `num_doc_proc` int(11) DEFAULT NULL,
  `nome_curso` varchar(45) DEFAULT NULL,
  `titulo_doc` varchar(255) DEFAULT NULL,
  `autor_doc` varchar(45) DEFAULT NULL,
  `procedencia` varchar(45) DEFAULT NULL,
  `destinatario` varchar(45) DEFAULT NULL,
  `setor_instituicao` varchar(45) DEFAULT NULL,
  `org_executor` varchar(45) DEFAULT NULL,
  `tipo_documental` varchar(45) DEFAULT NULL,
  `nivel_acesso` varchar(45) DEFAULT NULL,
  `dt_cronologica` varchar(45) DEFAULT NULL,
  `dt_topica` varchar(45) DEFAULT NULL,
  `classe_documental` varchar(45) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Fazendo dump de dados para tabela `documento`
--

INSERT INTO `documento` (`iddocumento`, `caixa_idcaixa`, `num_doc_proc`, `nome_curso`, `titulo_doc`, `autor_doc`, `procedencia`, `destinatario`, `setor_instituicao`, `org_executor`, `tipo_documental`, `nivel_acesso`, `dt_cronologica`, `dt_topica`, `classe_documental`) VALUES
(1, 1, 1, '1', 'Y', 'Y', 'Y', 'Y', 'Y', 'Y', 'Y', 'PÃºblico', '', '', 'G');

-- --------------------------------------------------------

--
-- Estrutura para tabela `usuario`
--

CREATE TABLE IF NOT EXISTS `usuario` (
`idusuario` int(11) NOT NULL,
  `login` varchar(150) DEFAULT NULL,
  `senha` varchar(150) DEFAULT NULL,
  `tipo` varchar(45) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Fazendo dump de dados para tabela `usuario`
--

INSERT INTO `usuario` (`idusuario`, `login`, `senha`, `tipo`) VALUES
(1, 'ADM', '1234', 'ADMIN'),
(2, 'JUAREZ', '123', 'USER');

-- --------------------------------------------------------

--
-- Estrutura para tabela `usuarios`
--

CREATE TABLE IF NOT EXISTS `usuarios` (
`idusuario` int(11) NOT NULL,
  `usuario` varchar(150) DEFAULT NULL,
  `senha` varchar(150) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

--
-- Fazendo dump de dados para tabela `usuarios`
--

INSERT INTO `usuarios` (`idusuario`, `usuario`, `senha`, `email`) VALUES
(3, 'allan', 'allan', 'allanc.rock@hotmail.com'),
(4, NULL, NULL, NULL),
(5, NULL, NULL, NULL),
(6, 'juca', 'juca', 'juca@inf.ufsm.br'),
(7, NULL, 'paula', 'paula@paula');

-- --------------------------------------------------------

--
-- Estrutura para tabela `usuario_has_documento`
--

CREATE TABLE IF NOT EXISTS `usuario_has_documento` (
  `usuario_idusuario` int(11) NOT NULL,
  `documento_iddocumento` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Índices de tabelas apagadas
--

--
-- Índices de tabela `caixa`
--
ALTER TABLE `caixa`
 ADD PRIMARY KEY (`idcaixa`);

--
-- Índices de tabela `documento`
--
ALTER TABLE `documento`
 ADD PRIMARY KEY (`iddocumento`), ADD KEY `fk_documento_caixa_idx` (`caixa_idcaixa`);

--
-- Índices de tabela `usuario`
--
ALTER TABLE `usuario`
 ADD PRIMARY KEY (`idusuario`);

--
-- Índices de tabela `usuarios`
--
ALTER TABLE `usuarios`
 ADD PRIMARY KEY (`idusuario`), ADD UNIQUE KEY `idusuario` (`idusuario`), ADD UNIQUE KEY `email` (`email`), ADD UNIQUE KEY `usuario` (`usuario`);

--
-- Índices de tabela `usuario_has_documento`
--
ALTER TABLE `usuario_has_documento`
 ADD PRIMARY KEY (`usuario_idusuario`,`documento_iddocumento`), ADD KEY `fk_usuario_has_documento_documento1_idx` (`documento_iddocumento`), ADD KEY `fk_usuario_has_documento_usuario1_idx` (`usuario_idusuario`);

--
-- AUTO_INCREMENT de tabelas apagadas
--

--
-- AUTO_INCREMENT de tabela `caixa`
--
ALTER TABLE `caixa`
MODIFY `idcaixa` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de tabela `documento`
--
ALTER TABLE `documento`
MODIFY `iddocumento` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de tabela `usuario`
--
ALTER TABLE `usuario`
MODIFY `idusuario` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de tabela `usuarios`
--
ALTER TABLE `usuarios`
MODIFY `idusuario` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- Restrições para dumps de tabelas
--

--
-- Restrições para tabelas `documento`
--
ALTER TABLE `documento`
ADD CONSTRAINT `fk_documento_caixa` FOREIGN KEY (`caixa_idcaixa`) REFERENCES `caixa` (`idcaixa`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Restrições para tabelas `usuario_has_documento`
--
ALTER TABLE `usuario_has_documento`
ADD CONSTRAINT `fk_usuario_has_documento_documento1` FOREIGN KEY (`documento_iddocumento`) REFERENCES `documento` (`iddocumento`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `fk_usuario_has_documento_usuario1` FOREIGN KEY (`usuario_idusuario`) REFERENCES `usuario` (`idusuario`) ON DELETE NO ACTION ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
