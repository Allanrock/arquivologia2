<?php
require 'db.php';
if (!empty($_POST)) {


    $caixa_id = $_POST['caixa_id'];
    $num_doc = $_POST['num_doc'];
    $nome_curso = $_POST['nome_curso'];
    $titulo_doc = $_POST['titulo_doc'];
    $autor_doc = $_POST['autor_doc'];
    $procedencia = $_POST['procedencia'];
    $destinatario = $_POST['destinatario'];
    $setor_inst = $_POST['setor_inst'];
    $org_executor = $_POST['org_executor'];
    $tipo_documento = $_POST['tipo_documento'];
    $nivel_acesso = $_POST['nivel_acesso'];
    $dt_crono = $_POST['dt_crono'];
    $dt_tipica = $_POST['dt_tipica'];
    $classe_doc = $_POST['classe_doc'];

    $valid = true;

    if (empty($caixa_id)) {
        $caixa_id_error = "Selecione uma caixa";
        $valid = false;
    }

    if (empty($num_doc)) {
        $num_doc_error = "Digite o número do documento";
        $valid = false;
    }

    if ($valid) {
        $PDO->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $sql = "insert into documento (caixa_idcaixa,num_doc_proc,nome_curso,titulo_doc,autor_doc,procedencia,destinatario,setor_instituicao,org_executor,tipo_documental,nivel_acesso,dt_cronologica,dt_topica,classe_documental) "
                . "values (?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        $stm = $PDO->prepare($sql);
        $stm->execute(array($caixa_id, $num_doc, $nome_curso, $titulo_doc, $autor_doc, $procedencia, $destinatario, $setor_inst, $org_executor, $tipo_documento, $nivel_acesso, $dt_crono, $dt_tipica, $classe_doc));
        $PDO = null;
        header("Location: index.php");
    }
}
?>
<!DOCTYPE html>
<html lang = "en">
    <head>
        <meta charset = "utf-8">
        <link href = "bootstrap/css/bootstrap.min.css" rel = "stylesheet">
        <script src = "bootstrap/js/bootstrap.min.js"></script>
    </head>

    <body>
        <?php include 'menu.php'?>
        <div class="container">

            <div class="row">
                <div class="row">
                    <h3>Inserir Documento</h3>
                </div>

                <form method="POST" action="">
                    <div class="form-group <?php echo!empty($caixa_id_error) ? 'has-error' : ''; ?>">
                        <label for="caixa_id">Caixa</label>
                        <select name="caixa_id" id="caixa_id">
                            <?php
                            $sql = "select idcaixa from caixa";
                            foreach ($PDO->query($sql) as $row) {
                                ?>
                                <option><?php echo $row['idcaixa'] ?></option>
                            <?php } ?>
                        </select>
                    </div>
                    <div class="form-group <?php echo!empty($num_doc_error) ? 'has-error' : ''; ?>">
                        <label for="num_doc">Número do Documento</label>
                        <input type="text" class="form-control" required="required" id="num_doc" value="<?php echo!empty($num_doc) ? $num_doc : ''; ?>" name="num_doc">

                    </div>
                    <div class="form-group">
                        <label for="nome_curso">Nome do Curso</label>
                        <input type="number" class="form-control" id="nome_curso" name="nome_curso">
                    </div>
                    <div class="form-group">
                        <label for="titulo_doc">Título do Documento</label>
                        <input type="text" name="titulo_doc" id="titulo_doc" class="form-control"/>
                    </div>
                    <div class="form-group">
                        <label for="autor_doc">Autor do Documento</label>
                        <input type="text" name="autor_doc" id="autor_doc" class="form-control"/>
                    </div>
                    <div class="form-group">
                        <label for="procedencia">Procedência</label>
                        <input type="text" name="procedencia" id="procedencia" class="form-control"/>
                    </div>
                    <div class="form-group">
                        <label for="destinatario">Destinatário</label>
                        <input type="text" name="destinatario" id="destinatario" class="form-control"/>
                    </div>
                    <div class="form-group">
                        <label for="setor_inst">Setor da Instituição</label>
                        <input type="text" name="setor_inst" id="setor_inst" class="form-control"/>
                    </div>
                    <div class="form-group">
                        <label for="org_executor">Órgão Executor</label>
                        <input type="text" name="org_executor" id="org_executor" class="form-control"/>
                    </div>
                    <div class="form-group">
                        <label for="tipo_documento">Tipo de Documento</label>
                        <input type="text" name="tipo_documento" id="tipo_documento" class="form-control"/>
                    </div>
                    <div class="form-group">
                        <label for="nivel_acesso">Nível de Acesso</label>
                        <select name="nivel_acesso" id="nivel_acesso" class="form-control">
                            <option value="Público">Público</option>
                            <option value="Sigiloso">Sigiloso</option>
                            <option value="Pessoal">Pessoal</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="dt_crono">Data Cronológica</label>
                        <input type="date" name="dt_crono" id="dt_crono" class="form-control"/>
                    </div>
                    <div class="form-group">
                        <label for="dt_tipica">Data Típica</label>
                        <input type="date" name="dt_tipica" id="dt_tipica" class="form-control"/>
                    </div>
                    <div class="form-group">
                        <label for="classe_doc">Classe do Documento</label>
                        <input type="text" name="classe_doc" id="classe_doc" class="form-control"/>
                    </div>

                    <div class="form-actions">
                        <button type="submit" class="btn btn-success">Inserir</button>
                        <a class="btn btn-default" href="index.php">Voltar</a>
                    </div>
                </form>

            </div> <!-- /row -->
        </div> <!-- /container -->

    </body>
</html>


