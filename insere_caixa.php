<?php
if (!empty($_POST)) {
    require 'db.php';

    $fundo = $_POST['fundo'];
    $secao = $_POST['secao'];
    $sub_secao = $_POST['sub_secao'];
    $serie = $_POST['serie'];
    $funcao = $_POST['funcao'];
    $classe = $_POST['classe'];
    $tipo_documento = $_POST['tipo_documento'];
    $caixacol = $_POST['caixacol'];
    $descritores = $_POST['descritores'];
    $qrcode = $_POST['qrcode'];

    $valid = true;
    
    if(empty($fundo)){
        $fundo_error = "Campo Fundo está em branco";
        $valid = false;
    }
    
    if(empty($secao)){
        $secao_error = "Insira uma seção";
        $valid = false;
    }

    if ($valid) {
        $PDO->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $sql = "insert into caixa (fundo,secao,sub_secao,serie,funcao,classe,tipos_documentos,caixacol,descritores,qrcode)"
                . " values (?,?,?,?,?,?,?,?,?,?)";
        $stm = $PDO->prepare($sql);
        $stm->execute(array($fundo,$secao,$sub_secao,$serie,$funcao,$classe,$tipo_documento,$caixacol,$descritores,$qrcode));
        $PDO = null;
        header("Location: index.php");
    }
}
?>

<!DOCTYPE html>
<html lang = "en">
    <head>
        <meta charset = "utf-8">
        <link href = "bootstrap/css/bootstrap.min.css" rel = "stylesheet">
        <script src = "bootstrap/js/bootstrap.min.js"></script>
    </head>

    <body>
        <?php include 'menu.php'?>
        <div class="container">

            <div class="row">
                <div class="row">
                    <h3>Inserir Caixa</h3>
                </div>

                <form method="POST" action="">
                    <div class="form-group <?php echo !empty($fundo_error)?'has-error':'';?>">
                        <label for="fundo">Fundo</label>
                        <input type="number" class="form-control" id="fundo" name="fundo">
                    </div>
                    <div class="form-group">
                        <label for="secao">Seção</label>
                        <input type="text" name="secao" id="secao" class="form-control"/>
                    </div>
                    <div class="form-group">
                        <label for="sub_secao">Sub Seção</label>
                        <input type="text" name="sub_secao" id="sub_secao" class="form-control"/>
                    </div>
                    <div class="form-group">
                        <label for="serie">Serie</label>
                        <input type="text" name="serie" id="serie" class="form-control"/>
                    </div>
                    <div class="form-group">
                        <label for="funcao">Função</label>
                        <input type="text" name="funcao" id="funcao" class="form-control"/>
                    </div>
                    <div class="form-group">
                        <label for="classe">Classe</label>
                        <input type="text" name="classe" id="classe" class="form-control"/>
                    </div>
                    <div class="form-group">
                        <label for="tipo_documento">Tipo de Documento</label>
                        <input type="text" name="tipo_documento" id="tipo_documento" class="form-control"/>
                    </div>
                    <div class="form-group">
                        <label for="caixacol">Caixa Col</label>
                        <input type="text" name="caixacol" id="caixacol" class="form-control"/>
                    </div>
                    <div class="form-group">
                        <label for="descritores">Descritores</label>
                        <input type="text" name="descritores" id="descritores" class="form-control"/>
                    </div>
                    <div class="form-group">
                        <label for="qrcode">QR Code</label>
                        <input type="text" name="qrcode" id="qrcode" class="form-control"/>
                    </div>

                    <div class="form-actions">
                        <button type="submit" class="btn btn-success">Inserir</button>
                        <a class="btn btn-default" href="index.php">Voltar</a>
                    </div>
                </form>

            </div> <!-- /row -->
        </div> <!-- /container -->
    </body>
</hmtl>
        